const mongoose = require("mongoose");

const Dish = mongoose.model(
  "Dish",
  new mongoose.Schema({
    title: {
      type: String, 
      // required: true
    },
    description: {
      type: String, 
      // required: true
    },
    price: {
      type: Number, 
      // required: true
    },
    image: {
      type: String,  
      // required: true
    },
    status: {
      type: String,
      enum: ['Active', 'NoPublished'],
      default: 'NoPublished'
    }
    // orders: [
    //   {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "Order"
    //   }
    // ]
  })
);

module.exports = Dish
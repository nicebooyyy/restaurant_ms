const mongoose = require("mongoose");

const Reservation = mongoose.model(
  "Reservation",
  new mongoose.Schema({
    confirmed:
    {
        type: String,
      enum: ['Yes', 'No'],
      default: 'No'
    },
    // dishes: [
    //     {
    //       type: mongoose.Schema.Types.ObjectId,
    //       ref: "Dish"
    //     }
    // ],
    tableId: {
        type: Number,
        required: true
    },
    // userId: {
    //      type: mongoose.Schema.Types.ObjectId,
    //      ref:"User"
    // },
    start: {
        type: Date, 
        required: true
    },
    end: {
        type: Date, 
        required: true
      },
    createdAt: {
        type: Date,
        default: Date.now
    },
    price: {
      type: Number, 
      required: true
    },
    status: {
      type: String,
      enum: ['Active', 'Done'],
      default: 'Active'
    }
  })
);

module.exports = Reservation
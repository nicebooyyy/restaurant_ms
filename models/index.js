const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.dish = require("./dish");
db.order = require("./order");
db.reservation = require("./reservation");

db.ROLES = ["user", "admin", "moderator","cook","waiter"];

module.exports = db;
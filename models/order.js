const mongoose = require("mongoose");

const OrderItemsSchema = new mongoose.Schema({
  _id: {
      //type: String,
      type: mongoose.Schema.Types.ObjectId,
      ref: "Dish",
      required: true
  },
  title: {
      type: String,
      required: true
  },
  description: {
    type: String,
    required: true
},
  price: {
      type: Number,
      required: true
  },
  quantity: {
    type: Number,
    required: true
    },
  createdAt: {
      type: Date,
      default: Date.now,
  },
  finishedAt: {
      type: Date
  },
  status: {
      type: String,
      enum: ['Active', 'Completed','NoPublished'],
      default: 'Active'
  }
});
const Order = mongoose.model(
  "Order",
  new mongoose.Schema({
      // dishes: [
      //   {
      //     type: mongoose.Schema.Types.ObjectId,
      //     ref: "Dish"
      //   }
      // ],
      tableId: {
        type: Number,
        required: true
      },
      status: {
          type: String,
          enum: ['InProgress', 'Completed'],
          default: 'InProgress'
        //   required: true
      },
      total: {
          type: Number
      },
      discount: {
          type: Number,
          default: 0
      },
      createdAt: {
          type: Date,
          default: Date.now
      },
      finishedAt: {
          type: Date
      },
      items: [
          OrderItemsSchema
      ]
    },
    // { timestamps: true }
  )
);


module.exports = Order;
const db = require("../models");
const Dish = db.dish;
//const Order = db.order;
// const fs = require('fs')

// const path = './uploads/profile_pic-1598365395508.jpg'

// try {
//   fs.unlinkSync(path)
//   //file removed
// } catch(err) {
//   console.error(err)
// }




exports.pagination = async(req, res) => {
    // destructure page and limit and set default values
    const { page = 1, limit = 10 } = req.query;
  
    try {
      // execute query with page and limit values
      const dishes = await Dish.find()
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
  
      // get total documents in the Dishes collection 
      const count = await Dish.countDocuments();
  
      // return response with posts, total pages, and current page
      res.json({
        dishes,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      });
    } catch (err) {
      console.error(err.message);
    }
  };

exports.list = (req, res) => {
  const dishes = Dish.find()
  .then((dishes) =>{
      res.status(200).json({
        dishes: dishes
      });
  })
  .catch(err => console.log(err))
};

// Retrieve all products from the database. ++
exports.findAll = (req, res) => {
  Dish.find()
  .then(dishes => {
      //res.send(dishes);
      res.status(200).json({
        dishes: dishes
    });
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Something wrong while retrieving dishes."
      });
  });
};

//Створення нової страви
exports.create = async(req, res, err) => {
    console.log(req.body);
    if (!req.file) {
        return res.send('Please select an image to upload');
    }
    
    const dish = new Dish({
        title: req.body.title || "Зі свининою та сиром дорблю", 
        description: req.body.description|| "Хрумка пшенична булочка, котлета зі свинини, салат айсберг, сир дорблю, томати та французький соус",
        price: req.body.price|| 99,
        image: req.file.path,
        status: "Active"
    });
    // Save Product in the database
    dish.save((err,result)=> {
              if (err) {
                  return res.status(400).json({
                      error:err
                  })
              }
              res.status(200).json({
                  dish: dish,
                  message: "Dish successfull add"
              });
    });
};




// Find a single product with a productId
exports.findOne = (req, res) => {
  Product.findById(req.params.productId)
  .then(product => {
      if(!product) {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });            
      }
      res.send(product);
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });                
      }
      return res.status(500).send({
          message: "Something wrong retrieving product with id " + req.params.productId
      });
  });
};

// Update a product
exports.update = (req, res) => {
  // Validate Request
  if(!req.body) {
      return res.status(400).send({
          message: "Product content can not be empty"
      });
  }

  // Find and update product with the request body
  Product.findByIdAndUpdate(req.params.productId, {
      title: req.body.title || "No product title", 
      description: req.body.description,
      price: req.body.price,
      company: req.body.company
  }, {new: true})
  .then(product => {
      if(!product) {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });
      }
      res.send(product);
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });                
      }
      return res.status(500).send({
          message: "Something wrong updating note with id " + req.params.productId
      });
  });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
  Product.findByIdAndRemove(req.params.productId)
  .then(product => {
      if(!product) {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });
      }
      res.send({message: "Product deleted successfully!"});
  }).catch(err => {
      if(err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
              message: "Product not found with id " + req.params.productId
          });                
      }
      return res.status(500).send({
          message: "Could not delete product with id " + req.params.productId
      });
  });
};
// const createDish = function(dish) {
//     return Dish.create(dish).then(docDish => {
//       console.log("\n>> Created Dish:\n", docDish);
//       return docDish;
//     });
//   };
  
  
 
//   const addDishToOrder = function(orderId, dish) {
//     return Order.findByIdAndUpdate(
//       orderId,
//       { $push: { dishes: dish._id } },
//       { new: true, useFindAndModify: false }
//     );
//   };
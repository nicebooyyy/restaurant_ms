exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
  };
  
  exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
  };
  
  exports.adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
  };
  
  exports.moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.");
  };

  exports.cookBoard = (req, res) => {
    res.status(200).send("Cook Content.");
  };

  exports.waiterBoard = (req, res) => {
    res.status(200).send("Waiter Content.");
  };
const db = require("../models");
const Dish = db.dish;
const Order = db.order;
  
// функція для групування продажів
exports.aggregateOrders = (req,res) => {
    const orders = Order.aggregate( 
      [ 
        // {
        //   $group :{ 
        //     _id : "Orders", 
        //     total_sum_orders: { $sum : "$total" }
        //   }
        // } 
        
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
            total: { $sum : "$total" },
            // count: { $sum : 1 },
            // year: {
            //   $year: '$createdAt'
            // },
          }
        },
        {
          $limit: 20,
        },
        { "$sort": { "_id": 1 } },
        
      ] 
    )
    
    .then((orders) =>{
        const ordersByDay = [];
        const dayByOrders = [];
        for (let index = 0; index < orders.length; index++) {
          ordersByDay.push(orders[index]["total"])
          dayByOrders.push(orders[index]["_id"])
        }
        console.log(ordersByDay);
        console.log(dayByOrders);
        res.status(200).json({
          orders: orders,
          ordersByDay: ordersByDay,
          dayByOrders: dayByOrders
        });
    })
    .catch(err => console.log(err))
};

exports.list = (req, res) => {
  const orders = Order.find().populate("dishes")
  .then((orders) =>{
      res.status(200).json({
        orders: orders
      });
  })
  .catch(err => console.log(err))
};

exports.createOrder = (req,res) => {
  const order = new Order(req.body)
  // console.log('create new order: ',req.body);
  order.save((err,result)=> {
      if (err) {
        console.log(err);
          return res.status(400).json({
              error:err
          })
      }
      res.status(200).json({
          //product: result
          message: "Order successfull add"
      });
  });
};

// DUMP
// exports.createOrder = (req,res) => {
//   const order = new Order(req.body)
//   console.log('create new order: ',req.body);
//   order.save((err,result)=> {
//       if (err) {
//           return res.status(400).json({
//               error:err
//           })
//       }
//       res.status(200).json({
//           //product: result
//           message: "Order successfull add"
//       });
//   });
// };

exports.listInProgress = (req, res,next) => {
      const orderStatus = "InProgress";
      const InProgress = Order.find({ status: orderStatus })
        .sort({ createdAt: -1 })
        .exec().then((InProgress) =>{
          res.status(200).json({
            InProgress
          });
        }).catch(err => console.log(err))
};
exports.listClosed = (req, res,next) => {
  const orderStatus = "Completed";
  const Completed = Order.find({ status: orderStatus })
    .sort({ createdAt: -1 })
    .exec().then((Completed) =>{
      res.status(200).json({
        Completed
      });
    }).catch(err => console.log(err))
}
// 
exports.updateOrder = (req, res,next) => {
  console.log(req.params);
  var id = req.params.id;
  var items = req.body.items;
  const updateOrder = Order.findOneAndUpdate(
          { "_id": id },
          { $push: { items: { $each: items } } },
          { new: true }
          )
          .exec()
          .then((updateOrder) =>{
            res.status(200).json({
              updateOrder
            });
          }).catch(err => console.log(err))
}

/**
 * Complete a single item on an order.
 */
exports.completeItem = (req, res,next) => {
  var orderId = req.params.id;
  var itemId = req.params.itemId;

  const OrderComplete = Order.findById(orderId)
        .exec()
        .then(order => {
            if (order) {
                return order;
            }
            // TODO: return error status code within error.
            return Promise.reject(new Error('No order found'));
        })
        ///
      .then(order => {
        var item = order.items.id(itemId);
        item.status = 'Completed';
          
          return order.save((err,result)=> {
            if (err) {
                return res.status(400).json({
                    error:err
                })
            }
            res.status(200).json({
              order: order
            });
        });
      })
      
}

/**
 * Handle completing an existing order.
 * TODO: validate there is no pending order items?
 */
exports.complete = (req, res,next) => {

  var orderId = req.params.id;
  var total = req.body.total;

  return Order.findById(orderId)
  .exec()
  .then(order => {
      if (order) {
          return order;
      }
      // TODO: return error status code within error.
      return Promise.reject(new Error('No order found'));
  })
  ///
      .then(order => {
        order.status = 'Completed';
        order.total = total;
        order.finishedAt = Date.now();
          return order.save((err,result)=> {
            if (err) {
                return res.status(400).json({
                    error:err
                })
            }
            res.status(200).json({
              order: order
            });
        });
      })
      
}




  // const createOrder = function(order) {
  //   return Order.create(order).then(docOrder => {
  //     console.log("\n>> Created Order:\n", docOrder);
  //     return docOrder;
  //   });
  // };
  
  // const addOrderToDish = function(dishId, order) {
  //   return Dish.findByIdAndUpdate(
  //       dishId,
  //     { $push: { orders: order._id } },
  //     { new: true, useFindAndModify: false }
  //   );
  // };


  // const orders = Order.aggregate(
    //   [{ $group : { 
    //        _id : { year: { $year : "$createdAt" }, month: { $month : "$createdAt" },day: { $dayOfMonth : "$createdAt" }}, 
    //        total : { $sum : 1 }}
    //        }, 
    //   { $group : { 
    //        _id : { year: "$_id.year", month: "$_id.month" }, 
    //        dailyusage: { $push: { day: "$_id.day", total: "$total" }}}
    //        }, 
    //   { $group : { 
    //        _id : { year: "$_id.year" }, 
    //        monthlyusage: { $push: { month: "$_id.month", dailyusage: "$dailyusage" }}}
    //        }, 
    //       ])
const CloudIpsp = require('cloudipsp-node-js-sdk')

const fondy = new CloudIpsp(
  {
    merchantId: 1396424,
    secretKey: 'test'
  }
)
exports.sendRequestFondy = (req,res) => {
    const requestData = {
    order_id: 'order_id_8',
    order_desc: 'test order',
    currency: 'USD',
    amount: '1000'
    }
    fondy.Checkout(requestData).then(data => {
        console.log(data);
        res.send(data);
    }).catch((error) => {
        console.log(error)
    })
};

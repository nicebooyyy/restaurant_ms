const paypal = require('paypal-rest-sdk');
const { payment } = require("paypal-rest-sdk");


paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AScj3E3gUM5KyOJs8PQsBRyZV4FTEKpoDOXaTgEeFIrgIjNHz_0vThyBqfmvZ8iBIs_0EqStYD9zxplb',
  'client_secret': 'ECwxMN4lQ-nAaPyI3F7kg2lJVkbGD5fxL6bGimnUoQAgIcyFb-AfbrjINFZwOqLZBjiU-eoODvUsA_2u'
});

exports.index = (req, res) => {
    console.log("Paypal hello");
    res.json({
        paypal: "hello"
    });
};
exports.pay = (req, res) => {
    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://localhost:3000/paypal/success",
            "cancel_url": "http://localhost:3000/paypal/cancel"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "item",
                    "sku": "item",
                    "price": "1.00",
                    "currency": "USD",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "USD",
                "total": "1.00"
            },
            "description": "This is the payment description."
        }]
    };
    //
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error;
        } else {
            console.log("Create Payment Response");
            console.log(payment);
            for (let i = 0; i < payment.links.length; i++) {
                if (payment.links[i].rel === "approval_url") {
                    res.redirect(payment.links[i].href);
                }
                
            }
            res.send(payment);
        }
    });
    
    // res.json({
    //     paypal: "pay"
    // });
};

exports.success = (req, res) => {
    const payerId = req.query.PayerId;
    const paymentId = req.query.paymentId;

    const execute_payment_json = {
        "payer_id": payerId,
        "transactions":[{
            "amount": {
                "currency": "USD",
                "total": "1.00"
            }
        }]
    };
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log("Get Payment Response");
            console.log(JSON.stringify(payment));
            res.send(payment);
        }
    });
    // console.log("Paypal success");
    // res.json({
    //     paypal: "success payments"
    // });
};

exports.cancel = (req, res) => {
    res.send("payment cancelled!!!error");
};
const public_key  = "sandbox_i35235521539";
const private_key  = "sandbox_yKvKdWevo6NzR4mByWAl6gJXCYLdszj4XPM6VHy1";

exports.sendRequestLiqpay = (req,res) => {
    var LiqPay = require('../lib/liqpay');
    var liqpay = new LiqPay(public_key, private_key);
    liqpay.api("request", {
        "sandbox":"1",
    "action"         : "pay",
    "version"        : "3",
    "phone"          : "380950000001",
    "amount"         : "1",
    "currency"       : "UAH",
    "description"    : "description text",
    "order_id"       : "order_id_5",
    "card"           : "4000000000003055",
    "language":"uk",
    "card_exp_month" : "03",
    "card_exp_year"  : "22",
    "card_cvv"       : "111"
    }, function( json ){
    console.log( json.status );
    res.send(json);
    });
};

exports.testRequest = (req,res) => {
    console.log(req.body);
    var LiqPay = require('../lib/liqpay');
    var liqpay = new LiqPay(public_key, private_key);
    const json_string = {
        "sandbox":"1",
        "version":"3",
        "action":"pay",
        "amount":req.body.total,
        "currency":"UAH",
        "description":"test",
        // "order_id":"000004",
        "order_id": Math.floor(1 + Math.random() * 900000000),
        // "result_url":"http://localhost:8080/#/done",
        "result_url":"http://localhost:3000/liqpay/success",
        "server_url":"http://localhost:3000/liqpay/success"
    }
    const data = liqpay.cnb_form(json_string);
    res.send(data);
    //res.send(req.body);
};

exports.sendRequest = (req,res) => {
    var LiqPay = require('../lib/liqpay');
    var liqpay = new LiqPay(public_key, private_key);
    const json_string = {
        "sandbox":"1",
        "version":"3",
        "action":"pay",
        "amount":"3000000",
        "currency":"UAH",
        "description":"test",
        "order_id":"000004",
        "result_url":"http://localhost:3000/liqpay/success"
    }
    const data = liqpay.cnb_form(json_string);
    res.send(data);

};

exports.LiqpaySuccess = (req,res) => {
    var LiqPay = require('../lib/liqpay');
    var liqpay = new LiqPay(public_key, private_key);

    const data = req.body.data;
    const signature = req.body.signature;
    //res.send(signature);
    //decode
    let buff = new Buffer(data, 'base64');
    let text = buff.toString('ascii');
    //format to json object
    const obj = JSON.parse(text);
    
    //check signature
    const sign = liqpay.str_to_sign(
        private_key +
        data +
        private_key
    );
    if (sign===signature) {
        const idSendVue = obj.order_id;
        liqpay.api("request", {
            "sandbox":"1",
            "action"   : "status",
            "version"  : "3",
            "order_id" : obj.order_id 
            //order_id payment_id liqpay_order_id
            }, function( json ){
                console.log( json.status );
                console.log( json );
                //res.send(json);
                //res.redirect('http://exmple.com'+req.url)
                res.redirect('http://localhost:8080/#/done?id='+idSendVue);
                //.send();
            });
    } else {
        res.json({
            error:"error signature"
        });
    }
    
    //no dell Info for Developer
    // res.json({
    //     data: data,
    //     encodedData:obj,
    //     // encodedData:obj.payment_id,
    //     signData: sign,
    //     signature: signature
    // });
};

exports.OrderDone = (req,res) => {
    var LiqPay = require('../lib/liqpay');
    var liqpay = new LiqPay(public_key, private_key);
    liqpay.api("request", {
        "sandbox":"1",
        "action"   : "status",
        "version"  : "3",
        "order_id" : req.body.total

        }, function( json ){
            //ДОДАТИ запис в бд якщо замовлення успішне АБО перевірку замовлення
            //console.log( json.status );
            //console.log( json );
            res.send(json);
            
        });
    };

    // exports.OrderDoneTest = (req,res) => {
    //     var LiqPay = require('../lib/liqpay');
    //     var liqpay = new LiqPay(public_key, private_key);
    //     liqpay.api("request", {
    //         "sandbox":"1",
    //         "action"   : "status",
    //         "version"  : "3",
    //         "order_id" : 396599335
    //         //512803294
    //         //1393251551
    //         }, function( json ){
    //             console.log( json.status );
    //             console.log( json );
    //             res.send(json);
                
    //         });
    //     };
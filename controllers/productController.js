const Product = require('../models/Product')

exports.getProduct = (req, res) => {
    const products = Product.find()
    .then((products) =>{
        res.status(200).json({
            products: products
        });
    })
    .catch(err => console.log(err))
};

exports.createProduct = (req,res) => {
    const product = new Product(req.body)
    //console.log('create new product: ',req.body);
    product.save((err,result)=> {
        if (err) {
            return res.status(400).json({
                error:err
            })
        }
        res.status(200).json({
            product: result
        });
    });
};

exports.findProduct = (req,res) => {
    const findProduct = Product.findById((req.params.productId), (err, Product)=>{
        if (err) {
            res.send(err);
        }
        res.json(Product);
    })
};

exports.updateProduct = (req,res) => {
    const updateProduct = Product.findOneAndUpdate(
        {_id: req.params.productId},
        req.body,
        {new:true},
        (err,updateProduct) => {
            if (err) {
                res.send(err);
            }
            res.json(updateProduct);
        })
};

exports.deleteProduct = (req,res) => {
    const deleteProduct = Product.remove(
        {_id: req.params.productId},
        (err) => {
            if (err) {
                res.send(err);
            }
            res.json({message:'Product removed success'});
        })
};
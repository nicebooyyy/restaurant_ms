zodiac = require("zodiac-ts");
const db = require("../models");
const Order = db.order;


exports.holt = (req, res) => {
    const orders = Order.aggregate( 
        [ 
          {
            $group: {
              _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
              total: { $sum : "$total" },
            }
          },
          {
            $limit: 20,
          },
          { "$sort": { "_id": 1 } },
          
        ] 
      )
      .then((orders) =>{
          const ordersByDay = [];
          for (let index = 0; index < orders.length; index++) {
            ordersByDay.push(orders[index]["total"])
          }
            var data = ordersByDay;
            var alpha = 0.4;
            var gamma = 0.3;
            var hs = new zodiac.HoltSmoothing(data, alpha, gamma)
            forecast2 = hs.predict(2);
            

            var arr3 = [];
            for(var i = 0; i < data.length; i++) {
                arr3.push([i,data[i]]);
            }
            var arr4 = [];
            for(var i = 0; i < forecast2.length; i++) {
                arr4.push([i,forecast2[i]]);
            }
            var arr5 = [];
            for(var i = 0; i < forecast2.length; i++) {
                arr5.push([i]);
            }

            // console.log(forecast2);
            res.status(200).json({
                arr3,
                arr4,
                data,
                forecast2,
                arr5,

            });
      })
      .catch(err => console.log(err))
};

exports.SimpleExponentialSmoothing = (req, res) => {
    const orders = Order.aggregate( 
        [ 
          {
            $group: {
              _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
              total: { $sum : "$total" },
            }
          },
          {
            $limit: 20,
          },
          { "$sort": { "_id": 1 } },
          
        ] 
      )
      .then((orders) =>{
          const ordersByDay = [];
          for (let index = 0; index < orders.length; index++) {
            ordersByDay.push(orders[index]["total"])
          }
            var data = ordersByDay;
            //var data = [25, 29, 24, 21,25, 29, 24, 21,25, 29, 24, 21];
            var alpha = 0.4;
            var ses = new zodiac.SimpleExponentialSmoothing(data, alpha);
            var forecast2 = ses.predict(1);
            
            var arr5 = [];
            for(var i = 0; i < forecast2.length; i++) {
                arr5.push([i]);
            }

            res.json({
                data,
                forecast2,
                arr5,
            });
        })
        .catch(err => console.log(err))
};


exports.DoubleExponentialSmoothing = (req, res) => {
    const orders = Order.aggregate( 
        [ 
          {
            $group: {
              _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
              total: { $sum : "$total" },
            }
          },
          {
            $limit: 20,
          },
          { "$sort": { "_id": 1 } },
          
        ] 
      )
      .then((orders) =>{
          const ordersByDay = [];
          for (let index = 0; index < orders.length; index++) {
            ordersByDay.push(orders[index]["total"])
          }
            var data = ordersByDay;
            //var data = [25, 29, 34, 21,25, 9, 24, 21,5, 29, 24, 21];
            var alpha = 0.4;
            var des = new zodiac.DoubleExponentialSmoothing(data, alpha);
            forecast2 = des.predict(2);
            
            var arr5 = [];
            for(var i = 0; i < forecast2.length; i++) {
                arr5.push([i]);
            }

            res.json({
                data,
                forecast2,
                arr5,
            });
        })
        .catch(err => console.log(err))
};

exports.Arima = (req, res) => {
    const orders = Order.aggregate( 
        [ 
          {
            $group: {
              _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
              total: { $sum : "$total" },
            }
          },
          {
            $limit: 20,
          },
          { "$sort": { "_id": 1 } },
          
        ] 
      )
      .then((orders) =>{
          const ordersByDay = [];
          for (let index = 0; index < orders.length; index++) {
            ordersByDay.push(orders[index]["total"])
          }
            var data = ordersByDay;
    const arima = require('arima')
    //var data = [25, 29, 34, 21,25, 9, 24, 21,5, 29, 24, 21];
    var count = data.length;
    count++;
    const [pred, errors] = arima(data, count, {
        method: 0, // ARIMA method (Default: 0)
        optimizer: 6, // Optimization method (Default: 6)
        p: 1, // Number of Autoregressive coefficients
        d: 0, // Number of times the series needs to be differenced
        q: 1, // Number of Moving Average Coefficients
        verbose: false // Output model analysis to console
    })
    //console.log(pred)
    
    var arr5 = [];
    for(var i = 0; i < pred.length; i++) {
        arr5.push([i]);
    }

    res.json({
        data,
        pred,
        arr5,
    });
})
.catch(err => console.log(err))
};




exports.HoltWintersSmoothing = (req, res) => {
    const orders = Order.aggregate( 
        [ 
          {
            $group: {
              _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
              total: { $sum : "$total" },
            }
          },
          {
            $limit: 20,
          },
          { "$sort": { "_id": 1 } },
          
        ] 
      )
      .then((orders) =>{
          const ordersByDay = [];
          for (let index = 0; index < orders.length; index++) {
            ordersByDay.push(orders[index]["total"])
          }
            var data = ordersByDay;
            //var data = [25, 29, 34, 21,25, 9, 24, 21,5, 29, 24, 21,25, 9, 34, 21,25, 9, 24, 216,5, 29, 24, 21];
    var alpha = 0.4;
    var gamma = 0.3;
    var delta = 0.5;
    //var seasonLength = 12;
    var seasonLength = 3;
    var multiplicative = false; //indicates if the model is additive or multiplicative
 
    var hws = new zodiac.HoltWintersSmoothing(data, alpha, gamma, delta, seasonLength, multiplicative);
    forecast2 = hws.predict(20);

    var arr5 = [];
    for(var i = 0; i < forecast2.length; i++) {
        arr5.push([i]);
    }

    res.json({
        data,
        forecast2,
        arr5,
    });
      })
      .catch(err => console.log(err))
};
# Project Title

Система керування рестораном з прогнозуванням продажів

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


## Built With

* [Node.js]
* [Vue.js]


## Versioning

Version 0.0.1

## Authors

* **Sergey Neroda** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

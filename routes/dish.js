const express  = require('express')
const DishController = require('../controllers/dishController')

const router = express.Router()
const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/dishes');
    },
    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});     
    

router.get('/dish',DishController.list)
// router.post('/dish',DishController.createDish)
router.get('/dishpagination', DishController.pagination)

// Upload filerouter.post('/upload', DishController.uploadTest,(err,req,res,next) =>res.status(404).send({error:err.message}));

// Create a new Product
router.post('/dishes',multer({ storage: storage }).single('file'), DishController.create);

// Retrieve all Products
router.get('/dishes', DishController.findAll);

// Retrieve a single Product with productId
router.get('/dishes/:productId', DishController.findOne);

// Update a Note with productId
router.put('/dishes/:productId', DishController.update);

// Delete a Note with productId
router.delete('/dishes/:productId', DishController.delete);

module.exports = router;
const express  = require('express')
const LiqpayController = require('../controllers/liqpayController')

const router = express.Router()

router.get('/liqpaytest',LiqpayController.sendRequestLiqpay)
router.get('/liqpay',LiqpayController.sendRequest)
router.post('/liqpay/success',LiqpayController.LiqpaySuccess)
router.post('/liqpay',LiqpayController.testRequest)
router.post('/liqpay/orderdone',LiqpayController.OrderDone)

//router.post('/liqpay/orderdonetest',LiqpayController.OrderDoneTest)
// router.post('/paypal',LiqpayController.pay)

module.exports = router;
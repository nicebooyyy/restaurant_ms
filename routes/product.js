const express  = require('express')
const ProductController = require('../controllers/productController')

const router = express.Router()

router.get('/product',ProductController.getProduct)
router.post('/product',ProductController.createProduct)
router.get('/product/:productId',ProductController.findProduct)
router.put('/product/:productId',ProductController.updateProduct)
router.delete('/product/:productId',ProductController.deleteProduct)

module.exports = router;
const express  = require('express')
const UserController = require('../controllers/userController')

const router = express.Router()

router.get('/user', UserController.user)

module.exports = router;

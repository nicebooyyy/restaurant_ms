const express  = require('express')
const OrderController = require('../controllers/orderController')

const router = express.Router()


   
    router.get('/order',OrderController.list)
    router.post('/order',OrderController.createOrder)
    router.get('/order/progress',OrderController.listInProgress)
    router.get('/order/closed',OrderController.listClosed)
    router.patch('/order/:id',OrderController.updateOrder)
    router.post('/order/:id/items/:itemId/complete',OrderController.completeItem)
    router.post('/order/:id/complete',OrderController.complete)
    //agregations
    router.get('/order/aggregate',OrderController.aggregateOrders)

    // router
    // .route('/:id/items/:itemId/complete')
    // /** POST /api/orders/:id/items/:itemId/complete - Mark an order item as completed */
    // .post(orderCtrl.completeItem);

module.exports = router;
const express  = require('express')
const PayPalController = require('../controllers/paypalController')

const router = express.Router()

router.get('/paypal',PayPalController.index)
router.post('/paypal',PayPalController.pay)
router.get('/paypal/success',PayPalController.success)
router.get('/paypal/cancel',PayPalController.cancel)

module.exports = router;
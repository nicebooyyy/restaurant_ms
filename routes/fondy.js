const express  = require('express')
const FondyController = require('../controllers/fondyController')

const router = express.Router()

router.get('/fondy',FondyController.sendRequestFondy)
// router.post('/paypal',LiqpayController.pay)

module.exports = router;
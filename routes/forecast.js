//const express  = require('express')
const { authJwt } = require("../middlewares");
const ForecastController = require('../controllers/forecastController')

//const router = express.Router()
module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });

    app.get(
        '/forecast/holt',
        [authJwt.verifyToken, authJwt.isAdmin],
        ForecastController.holt
        );

        app.post(
            '/forecast/holt',
            [authJwt.verifyToken, authJwt.isAdmin],
            ForecastController.holt
            );

    app.get(
        '/forecast/holt-winters',
        [authJwt.verifyToken, authJwt.isAdmin],
        ForecastController.HoltWintersSmoothing
        );

    app.get(
        '/forecast/ses',
        [authJwt.verifyToken],
        ForecastController.SimpleExponentialSmoothing
        );

    app.get(
        '/forecast/des',
        [authJwt.verifyToken, authJwt.isAdmin],
        ForecastController.DoubleExponentialSmoothing
        );

    app.get(
        '/forecast/arima',
        [authJwt.verifyToken, authJwt.isAdmin],
        ForecastController.Arima
    );

};
//module.exports = router;
const express  = require('express');

const PORT  = process.env.PORT || 3000;
const app = express();
const morgan  = require("morgan");
const dotenv = require('dotenv');
const mongoose  = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
//
const path = require('path');
const fs = require('fs');



dotenv.config()
var corsOptions = {
    origin: "http://localhost:8080"
};
const db = require("./models");
const Role = db.role;


//middleware
const MyMiddleware = (req, res, next) => { 
    console.log('middleware applied');
    next();
};


app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cors(corsOptions));

//додаю статичну папку
app.use('/uploads', express.static(__dirname + '/uploads'));

//створюю папки

  const dishDir = path.join(__dirname, 'uploads/dishes');
  const userDish = path.join(__dirname, 'uploads/users');

  try {
    fs.mkdirSync(dishDir,{recursive: true});
    fs.mkdirSync(userDish, {recursive: true});
  } catch (error) {
    console.log(error);
  };


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true,
  json: {limit: '50mb', extended: true},
  urlencoded: {limit: '50mb', extended: true} }));

//routes export
const HomeRoutes = require('./routes/home')
const UserRoutes = require('./routes/user');
const ProductRoutes = require('./routes/product');
const AuthRoutes = require('./routes/auth');
const OrderRoutes = require('./routes/order');
const DishRoutes = require('./routes/dish');
const PayPalRoutes = require('./routes/paypal');
const LiqPayRoutes = require('./routes/liqpay');
const FondyRoutes = require('./routes/fondy');
//const ForecastRoutes = require('./routes/forecast')

app.use('/', HomeRoutes);
app.use('/', UserRoutes);
app.use('/', ProductRoutes);
app.use('/', AuthRoutes);
app.use('/', OrderRoutes);
app.use('/', DishRoutes);
app.use('/', PayPalRoutes);
app.use('/', LiqPayRoutes);
app.use('/', FondyRoutes);
//app.use('/', ForecastRoutes);
// routes
require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);
require('./routes/forecast')(app);

// Handle 404 - Keep this as a last route
app.use(function(req, res, next) {
  res.status(403);
  res.send('403: Нема доступу!');
});
app.use(function(req, res, next) {
  res.status(404);
  res.send('404: Сторінка не існує!');
});




//Старт сервера
async function start () {
    try {
       // підключення бд
       await mongoose.connect(process.env.MONGO_URI, {
           useNewUrlParser: true,
           useFindAndModify: false,
           useUnifiedTopology: true
       }) .then(() => {
        initial();
        console.log("Successfully connect to MongoDB.");
        
      })
      .catch(err => {
        console.error("Connection error", err);
        process.exit();
      });
       //.then(()=>console.log("Базу даних під'єднано!!!")) ;
    //    mongoose.connection.on('error', err => {
    //        console.log(`DB помилка з'єднання: ${err.message}`)
    //    });
       //старт сервера
        app.listen(PORT, () => {
            console.log(`Сервер запущено!!!(Port: ${PORT})`);
        })
    } catch (error) {
        console.log(error)
    }
}
//Викликаю функцію
start()

function initial() {
    Role.estimatedDocumentCount((err, count) => {
      if (!err && count === 0) {
        new Role({
          name: "user"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'user' to roles collection");
        });
  
        new Role({
          name: "moderator"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'moderator' to roles collection");
        });
  
        new Role({
          name: "admin"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'admin' to roles collection");
        });

        new Role({
          name: "cook"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'cook' to roles collection");
        });

        new Role({
          name: "waiter"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'waiter' to roles collection");
        });
      }
    });
  }
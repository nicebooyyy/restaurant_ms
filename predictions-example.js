//////////////////////////////////////////////////////
zodiac = require("zodiac-ts");
var data = [25, 29, 24, 21];

//Case of simple exponential smoothing////////////////1
var alpha = 0.4;

var ses = new zodiac.SimpleExponentialSmoothing(data, alpha);
var forecast = ses.predict(); //return an array with estimated values until t+1
// console.log(forecast)
//You can optimize alpha value
var optimizedAlpha = ses.optimizeParameter(20); //You have to pass the number of iterations as parameter
//After the optimization, the value of alpha is directly set to optimizedAlpha

//You can predict again with the optimized value of alpha
var optimizedForecast = ses.predict();
// console.log(optimizedForecast)

//Case of double exponential smoothing////////////////2
var des = new zodiac.DoubleExponentialSmoothing(data, alpha);
forecast1 = des.predict(3); //You have to pass the horizon of the prediction
// console.log(forecast1)
//You can also optimize alpha value
optimizedAlpha = des.optimizeParameter(20);
//After the optimization, the value of alpha is directly set to optimizedAlpha
var optimizedForecast1 = des.predict();
// console.log(optimizedForecast1)

//Case of Holt smoothing//////////////////////////////3
var gamma = 0.3;
var hs = new zodiac.HoltSmoothing(data, alpha, gamma)
forecast2 = hs.predict();
// console.log(forecast2);
var optimizedParameters = hs.optimizeParameters(20); //return an object containing the optimized value of alpha and gamma
//After the optimization, the value of alpha and gamma are directly set to the optimized values
var optimizedForecast2 = hs.predict();
// console.log(optimizedForecast2)

//Case of Holt Winters smoothing//////////////////////4
var delta = 0.5;
var seasonLength = 4;
var multiplicative = false; //indicates if the model is additive or multiplicative
 
var hws = new zodiac.HoltWintersSmoothing(data, alpha, gamma, delta, seasonLength, multiplicative);
forecast3 = hws.predict();
// console.log(forecast3);
optimizedParameters = hws.optimizeParameters(20); //return an object containing the optimized values of alpha, gamma and delta
//After the optimization, the value of alpha, gamma and delta are directly set to the optimized values
var optimizedForecast3 = hws.predict();
// console.log(optimizedForecast3)

/////////////ARIMA///////////////////////////////////5
// //The mean is taken from an equal number (order) of data on either side of a central value 
const arima = require('arima')
var ts = [25, 29, 24, 28];
const [pred, errors] = arima(ts, 4, {
  method: 0, // ARIMA method (Default: 0)
  optimizer: 6, // Optimization method (Default: 6)
  p: 1, // Number of Autoregressive coefficients
  d: 0, // Number of times the series needs to be differenced
  q: 1, // Number of Moving Average Coefficients
  verbose: false // Output model analysis to console
})
//console.log(pred)
//////////////////////////////////////////////////////